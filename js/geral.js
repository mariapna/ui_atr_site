(function(){

	$(document).ready(function(){
		$('.carrossel-parceiros').owlCarousel({
			items : 5,
			dots: false,
			loop: true,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			center: false,
			responsiveClass:true,			    
			responsive:{
				320:{
					items:1,

				},
				425:{
					items:3,

				},
				768:{
					items:4,

				},
				991:{
					items:5,

				},
			}
		});

		$("#carrossel-timeline").owlCarousel({
			items: 2,
			dots: false,
			loop: false,
			margin: 24,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			autoWidth: false,
			center: true,
			nav: true,
		});

		$('.carrossel-galeria').owlCarousel({
			items: 2,
			dots: false,
			loop: true,
			margin: 60,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			center: true,
		});

		$('.carrossel-destaque-menor').owlCarousel({
			items: 1,
			dots: true,
			nav: false,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: true,	
			smartSpeed: 450,
			animateOut: 'fadeOut',
		});

		$('.carrossel-destaque').owlCarousel({
			items: 1,
			dots: true,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: true,	
			smartSpeed: 450,
			singleItem : true,
			animateOut: 'fadeOut',
		});
	});

	$('.pg-sobre .secao-destaque .destaque-menor ul li').click(function(){
		let dataId = $(this).attr('data-id');

		$('.pg-sobre .secao-destaque .item').removeClass('active-item');
		$('.pg-sobre .secao-destaque #' + dataId + '.item').addClass('active-item');
	});

	$('.pg-projeto .secao-plantas article table tr').click(function(){
		let dataImage = $(this).attr('data-image');

		$('.pg-projeto .secao-plantas article table tr').removeClass('tr-active');
		$(this).addClass('tr-active');

		$('.pg-projeto .secao-plantas .planta img').attr('src', dataImage);
	});

	$('.pg-projeto .secao-galeria nav a').click(function(e){
		e.preventDefault();

		let dataId = $(this).attr('data-id');
		$('.pg-projeto .secao-galeria nav a').removeClass('galeria-ativa');
		$(this).addClass('galeria-ativa');

		$('.pg-projeto .secao-galeria .carrossel-galeria').removeClass('carrossel-galeria-active');
		$('.pg-projeto .secao-galeria #' + dataId + '.carrossel-galeria').addClass('carrossel-galeria-active');
	});

	$('a.scrollTop').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}

		}
	});


	$('header img.icone-menu').click(function(){
		$('header .menu-principal').addClass('menu-mobile');
	});

	$('header .menu-principal .icone-fechar-menu').click(function(){
		$('header .menu-principal').removeClass('menu-mobile');
	});



	// var userFeed = new Instafeed({
 //       get: 'user',
 //       userId: '11635157636',
 //       clientId: '3fbc1cde73aa4fa49f5c4e9e74f97803',
 //       accessToken: '11635157636.1677ed0.702d4869149b423f889d49359ebe4e6a',
 //       resolution: 'standard_resolution',
 //       template: '<a href="{{link}}" target="_blank" id="{{id}}"><div class="itemInstagram" style="background:url({{image}})"><small class="likeComments"><span class="likes">{{likes}}</span><span class="comments">{{comments}}</span></small></div></a>',
 //       sortBy: 'most-recent',
 //       limit: 6,
 //       links: false
 //     });
 //     userFeed.run();

}());